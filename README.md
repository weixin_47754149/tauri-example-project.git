# tauri-example-project
这是一个tauri 2.0的测试案例项目，个人构建，旨在提供一个试验tauri各种功能的环境，方便后面学习和使用。

# 安装运行
```bash
git clone https://gitcode.com/weixin_47754149/tauri-example-project.git
cd tauri-example-project
# 安装依赖
pnpm install
# 运行
pnpm tauri dev
```